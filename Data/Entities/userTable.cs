﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class userTable
    {
        [Key]
        public string userId { get; set; }
        [DataType("varchar(20)")]
        public string userName { get; set; }
        [DataType("varchar(20)")]
        public string firstName { get; set; }
        [DataType("varchar(20)")]
        public string password { get; set; }
        [DataType("integer")]
        public int roleId { get; set; }
        [DataType("integer")]
        public int status { get; set; }
        [DataType("integer")]
        public long Phonenumber { get; set; }
        [ForeignKey("roleId")]
        public virtual roleTable roleTable { get; set; }

    }
}

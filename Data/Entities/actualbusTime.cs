﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class actualbusTime
    {
        [Key]
        [DataType("varchar(20)")]
        public string busId { set; get; }
        [DataType("DateTime")]
        public string starttime { get; set; }
        [DataType("DateTime")]
        public string endtime { get; set; }
    }
}

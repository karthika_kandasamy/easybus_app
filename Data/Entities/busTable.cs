﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Entities
{
    public class busTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        [DataType("int")]
        public string busId { get; set; }

        [Required]
        [MinLength(9)]
        [MaxLength(12)]
        [DataType("nvarchar(12)")]
        public string busNo { get; set; }

        [Required]
        public int routeId { get; set; }
        [DataType("int")]
        public int status { get; set; }
        [ForeignKey("routeId")]
        public virtual routeTable routeTable { get; set; }



    }
}

﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Entities
{
    public class studentTable
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Required]
        public string userId { get; set; }

        [Required]
        [DataType("varchar(20)")]
        public string studentName { get; set; }
        
        [Required]
        [DataType("varchar(20)")]
        public string parentName { get; set; }
        [Required]
        [DataType("varchar(20)")]
        public long parentPhonenumber { get; set; }

        [DataType("integer")]
        [Required]
        public string busId { get; set; }

        [ForeignKey("busId")]
        public busTable busTable { get; set; }

        [Required]
        [DataType("integer")]
        public int departmentId { get; set; }

        [ForeignKey("departmentId")]
        public virtual departmentTable departmentTable { get; set; }
    }
}

﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class departmentOperation
    {
        dataContext dc = new dataContext();
        public bool savedepartmentTable(departmentDTO model)
        {
            try
            {
                departmentTable dept = new departmentTable();
                dept = dc.departmentTable.FirstOrDefault(x=>x.deptname==model.deptname);
                if (dept == null)
                {
                    departmentTable result = new departmentTable() { deptno = model.deptno, deptname = model.deptname };
                    dc.departmentTable.Add(result);
                    dc.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                return false;
            }
}
        public IEnumerable<departmentDTO> GetAlldepartment()
        {
            IEnumerable<departmentDTO> result = dc.departmentTable.Select(s => new departmentDTO()
            {
                deptno = s.deptno,
                deptname = s.deptname
            }).ToList();

            return result;
        }
        public bool deletedepartmentTable(int deptno)
        {
            try {
                var count = dc.studentTable.Count(u => u.departmentId == deptno);
                if (count == 0)
                {
                    var rt = dc.departmentTable.FirstOrDefault(x => x.deptno == deptno);
                    dc.departmentTable.Remove(rt);
                    dc.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool editdepartmentTable(int deptno)
        {

            return true;
        }

        public departmentDTO Editdepartment(int deptno)
        {
            var s = dc.departmentTable.FirstOrDefault(x => x.deptno == deptno);
            departmentDTO dto = new departmentDTO();
            dto.deptno = s.deptno;
            dto.deptname = s.deptname;
            return dto;

        }
        public bool updatedepartment(departmentDTO dto)
        {
            departmentTable r = new departmentTable() { deptno = dto.deptno, deptname = dto.deptname };
            dc.departmentTable.Attach(r);
            dc.Entry(r).State = EntityState.Modified;
            dc.SaveChanges();
            return true;


        }
    }
}



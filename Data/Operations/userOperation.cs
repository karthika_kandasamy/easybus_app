﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class userOperation
    {

        dataContext ctx = new dataContext();

        public bool SaveUser(userDTO model)
        {
            try
            {
                var user = new userTable()
                {
                    userId = model.userId,
                    userName = model.userName,
                    firstName = model.firstName,
                    password = model.password,
                    roleId = model.roleId,
                    Phonenumber = model.phoneNumber
                };
                ctx.userTable.Attach(user);
                ctx.Entry(user).State = EntityState.Modified;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;

            }
        }
        public bool AddUser(userDTO model)
        {
            try
            {
                var user = new userTable()
                {
                    userId = model.userId,
                    userName = model.userName,
                    firstName = model.firstName,
                    password = model.password,
                    roleId = model.roleId,
                    Phonenumber = model.phoneNumber,
                    status = 0
                };
                ctx.userTable.Add(user);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public IEnumerable<userDetails> GetAllUser()
        {
            List<userDetails> userdetails = new List<userDetails>();
            IEnumerable<userDTO> user = ctx.userTable.Select(s => new userDTO()
            {
                userId = s.userId,
                userName = s.userName,
                firstName = s.firstName,
                password = s.password,
                phoneNumber = s.Phonenumber,
                roleId = s.roleId
            }).ToList();

            IEnumerable<roleDTO> roles = ctx.roleTable.Select(s => new roleDTO()
            {
                roleno = s.roleno,
                rolename = s.rolename
            }).ToList();

            foreach (var user1 in user)
            {
                foreach (var role1 in roles)
                {
                    if (user1.roleId == role1.roleno)
                    {
                        userdetails.Add(new userDetails { userId = user1.userId, userName = user1.userName, fullName = user1.firstName, password = user1.password, role = role1.rolename, phoneNumber = user1.phoneNumber });
                    }
                }
            }
            //foreach (var i in user)
            //{
            //    if (i.roleId == 1)
            //    {
            //        userdetails.Add(new userDetails { userId = i.userId, userName = i.userName, fullName = i.firstName, password = i.password, role = "Admin", phoneNumber = i.phoneNumber });
            //    }
            //    else if (i.roleId == 2)
            //    {
            //        userdetails.Add(new userDetails { userId = i.userId, userName = i.userName, fullName = i.firstName, password = i.password, role = "Driver", phoneNumber = i.phoneNumber });
            //    }
            //    else
            //    {
            //        userdetails.Add(new userDetails { userId = i.userId, userName = i.userName, fullName = i.firstName, password = i.password, role = "Student", phoneNumber = i.phoneNumber });
            //    }
            //}
            return userdetails;
        }
        public bool DeleteUser(string userId)
        {
            try
            {
                var user = ctx.userTable.FirstOrDefault(m => m.userId == userId);
                ctx.userTable.Remove(user);
                ctx.SaveChanges();

                var student = ctx.studentTable.FirstOrDefault(m => m.userId == userId);
                ctx.studentTable.Remove(student);
                ctx.SaveChanges();
                
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public userDTO EditUser(string userId)
        {
            try
            {
                var user = ctx.userTable.FirstOrDefault(m => m.userId == userId);
                var useredit = new userDTO()
                {
                    userId = user.userId,
                    userName = user.userName,
                    firstName = user.firstName,
                    password = user.password,
                    roleId = user.roleId,
                    phoneNumber = user.Phonenumber
                };
                ctx.userTable.Attach(user);
                ctx.Entry(user).State = EntityState.Modified;
                ctx.SaveChanges();

                return useredit;
            }
            catch (Exception e)
            {
                return null;
            }

        }
        public bool statusupdate(string userId)
        {
            try
            {
                userTable ut = ctx.userTable.FirstOrDefault(x => x.userId == userId);
                ut.status = 1;
                ctx.userTable.Attach(ut);
                ctx.Entry(ut).State = EntityState.Modified;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public bool statusreverse(string username)
        {
            try
            {
                userTable ut = ctx.userTable.FirstOrDefault(x => x.userName == username);
                ut.status = 0;
                ctx.userTable.Attach(ut);
                ctx.Entry(ut).State = EntityState.Modified;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public string getRoleName(int roleId)
        {
            try
            {
                var ut = ctx.roleTable.FirstOrDefault(x => x.roleno == roleId);
                string rolename = ut.rolename;
                return rolename;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public bool checkExistance(string userId)
        {
            try
            {
                IEnumerable<studentDTO> user = ctx.studentTable.Select(s => new studentDTO()
                {
                    userId = s.userId
                    
                }).ToList();

                foreach(var id in user)
                {
                    if (id.userId == userId)
                        return true;
                }

                return false;
            }
            catch(Exception e)
            {
                return false;
            }
        }

        public IEnumerable<loggedInUserDTO> unlock()
        {

            IEnumerable<loggedInUserDTO> users = ctx.userTable.Select(s => new loggedInUserDTO()
            {
                userId = s.userId,
                userName = s.userName,
                roleId = s.roleId,
                status = s.status
            }).ToList();
            return users;
        }

        public bool updateuserStatus(String userId)
        {
            try
            {
                var br = ctx.userTable.FirstOrDefault(x => x.userId.Equals(userId));
                br.status = 0;
                ctx.userTable.Attach(br);
                ctx.Entry(br).State = EntityState.Modified;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}
public class userDetails
{
    [Display(Name = "User ID")]
    public string userId { get; set; }
    [Display(Name = "UserName")]
    public string userName { get; set; }
    [Display(Name = "Full Name")]
    public string fullName { get; set; }
    public string password { get; set; }
    [Display(Name = "Role")]
    public string role { get; set; }
    [Display(Name = "Phone Number")]
    public long phoneNumber { get; set; }
}
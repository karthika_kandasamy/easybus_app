﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class routeOperation
    {
            dataContext db = new dataContext();
            public Boolean Add(routeDTO model)
            {
            try
            {
                routeTable dept = new routeTable();
                dept = db.routeTable.FirstOrDefault(x => x.source == model.source&&x.destination==model.destination&&x.via==model.via);
                if (dept == null)
                {
                    routeTable rt = new routeTable { routeId = model.routeId, destination = model.destination, via = model.via, source = model.source, plannedTime = model.plannedTime };
                    db.routeTable.Add(rt);
                    db.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception e)
            {
                return false;
            }
            }
            public IList<routeDTO> Display()
            {
                IList<routeDTO> ilist = db.routeTable.Select(s => new routeDTO()
                {
                    routeId = s.routeId,
                    destination = s.destination,
                    source = s.source,
                    plannedTime = s.plannedTime,
                    via = s.via


                }).ToList();
                return ilist;
            }
            public bool delete(int routeId)
            {

            try
            {
                var count = db.busTable.Count(u => u.routeId == routeId);
                if (count == 0)
                {
                    var del = db.routeTable.FirstOrDefault(x => x.routeId == routeId);
                    db.routeTable.Remove(del);
                    db.SaveChanges();
                    return true;
                }
                else
                    return false;
            }
            catch(Exception e)
            {
                return false;
            }
            }
            public routeDTO Editroute(int routeId)
            {
                var s = db.routeTable.FirstOrDefault(x => x.routeId == routeId);
                routeDTO dto = new routeDTO();
                dto.routeId = s.routeId;
                dto.destination = s.destination;
                dto.via = s.via;
                dto.plannedTime = s.plannedTime;
                dto.source = s.source;
            return dto;

            }
            public bool updateroute(routeDTO dto)
            {
            try
            {
                routeTable r = new routeTable() { routeId = dto.routeId, source = dto.source, plannedTime = dto.plannedTime, destination = dto.destination, via = dto.via };
                db.routeTable.Attach(r);
                db.Entry(r).State = EntityState.Modified;
                db.SaveChanges();
                return true;
            }
            catch(Exception e)
            {
                return false;
            }
            }
        }
    }
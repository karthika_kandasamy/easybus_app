﻿using Data.DTO;
using Data.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.Operations
{
    public class locationOperation
    {
        dataContext dc = new dataContext();

        public IEnumerable<locationDTO> getlocationDetails()
        {
            IEnumerable<locationDTO> location = dc.locationTable.Select(s => new locationDTO()
            {
               
                busId = s.busId,
                userId = s.userId,
                latitude = s.latitude,
                longitude=s.longitude,
                dateTime=s.dateTime
            }).ToList();

            return location;
        }
        public bool addLocationDetails(locationDTO savemodel)
        {
            try
            {
                locationTable record = new locationTable() {busId = savemodel.busId, latitude = savemodel.latitude, longitude = savemodel.longitude,userId=savemodel.userId,dateTime=savemodel.dateTime };
                dc.locationTable.Add(record);
                dc.SaveChanges();
                return true;
            }
            catch (Exception err)
            {
                return false;
            }
        }

    }
}

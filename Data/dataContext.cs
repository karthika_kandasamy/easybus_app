﻿using Data.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

using System.Text;
using System.Threading.Tasks;

namespace Data
{
    public class dataContext:DbContext
    {
        public dataContext():base("easybus5")
        {
            Database.SetInitializer<dataContext>(new CreateDatabaseIfNotExists<dataContext>());
        }
        public DbSet<busTable> busTable { get; set; }
        public DbSet<departmentTable> departmentTable { get; set; }
        public DbSet<locationTable> locationTable { get; set; }
        public DbSet<roleTable> roleTable { get; set; }
        public DbSet<routeTable> routeTable { get; set; }
        public DbSet<studentTable> studentTable { get; set; }
        public DbSet<userTable> userTable { get; set; }
        public DbSet<alternateBusTable> alternateBusTable { get; set; }
        public DbSet<actualbusTime> actualbusTimes { get; set; }
    }
}

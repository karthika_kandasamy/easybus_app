﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class loggedInUserDTO
    {
        [Display(Name = "User ID")]
        public String userId { get; set; }
        [Display(Name = "User Name")]
        public string userName { get; set; }
        [Display(Name = "Name")]
        public string firstName { get; set; }
        [Display(Name = "Password")]
        public string password { get; set; }
        [Display(Name = "Role")]
        public int roleId { get; set; }
        [Display(Name = "ContactNumber")]
        public long phoneNumber { get; set; }
        public int status { get; set; }

    }
}

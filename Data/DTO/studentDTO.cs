﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class studentDTO
    {
        [Display(Name = "User ID")]
        public string userId { get; set; }
        [Display(Name = "Bus Number")]
        public string busId { get; set; }
        [Display(Name = "Name")]
        public string studentName { get; set; }
        [Display(Name = "Department")]
        public int departmentId { get; set; }
        [Display(Name = "Parent Name")]
        public string parentName { get; set; }
        [Display(Name = "Parents ContactNumber")]
        public long parentPhonenumber { get; set; }
    }
}

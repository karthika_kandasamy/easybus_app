﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class locationDTO
    {
        [Display(Name = "Bus Number")]
        public string busId { get; set; }
        [Display(Name = "User ID")]
        public string userId { get; set; }
        [Display(Name = "Latitude")]
        public double latitude { get; set; }
        [Display(Name = "Longitude")]
        public double longitude { get; set; }
        [Display(Name = "Date & Time")]
        public DateTime dateTime { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Data.DTO
{
    public class roleDTO
    {
        [Display(Name = " Role Number")]
        public int roleno { get; set; }
        [Display(Name = "Role Name")]
        public string rolename { get; set; }
    }
}

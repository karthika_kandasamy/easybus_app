﻿using Data;
using Data.DTO;
using Data.Entities;
using Data.Operations;
using easyBus2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace easyBus2.Controllers
{
    public class homeController : Controller
    {
        // GET: home
        dataContext dc = new dataContext();
        HttpCookie mycookie = new HttpCookie("userNameCookie");

        public ActionResult pageload()
        {
            HttpCookie newCookie = Request.Cookies["userNameCookie"];
            if (newCookie == null)
                return RedirectToAction("Index");
            else
            {
                if (Request.Cookies["userNameCookie"].Value == "null")
                    return RedirectToAction("Index");
                else
                    return RedirectToAction("busView");
            }

        }
        public ActionResult Index()
        {
            
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(adminvalidate adm)
        {
            if (Session["userName"] == null)
            {
                if (ModelState.IsValid)
                {
                    mycookie.Value = adm.userName;
                    mycookie.Expires=DateTime.Now.AddYears(1);
                    HttpContext.Response.SetCookie(mycookie);
                    var dbs = dc.userTable.Where(x => x.userName == adm.userName).ToArray();
                    if (dbs.Any(u => u.userName == adm.userName && u.password == adm.password ))
                    {
                        var d = dc.userTable.FirstOrDefault(x => x.userName == adm.userName);
                        Session["userName"] = adm.userName;
                        Session["password"] = adm.password;
                        userOperation oper = new userOperation();
                        var result = oper.statusupdate(d.userId);
                        return RedirectToAction("busView");
                    }
                    else
                    {
                        //ModelState.AddModelError("password", "The username or password is incorrect");
                        TempData["Message"] = "UserName or Password is incorrect";
                        return View();

                    }
                }
                else
                {
                    // ModelState.AddModelError("", "The user name or password provided is incorrect.");
                    return RedirectToAction("Index");
                }
            }
            else
            {
                return RedirectToAction("busView");
            }
        }
        public ActionResult adminpage()
        {
            try
            {
                Session["userName"] = Request.Cookies["userNameCookie"].Value.ToString();
                return View();
            }
            catch(Exception err)
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult logOut()
        {
            try
            {
                FormsAuthentication.SignOut();

                var username = Convert.ToString(Session["userName"]);
                mycookie.Value = "null";
                HttpContext.Response.SetCookie(mycookie);
                userOperation oper = new userOperation();
                oper.statusreverse(username);
                Session.Abandon();
                return RedirectToAction("Index", "home");
            }
            catch(Exception err)
            {
                FormsAuthentication.SignOut();
                return RedirectToAction("Index", "home");
            }
        }


        public ActionResult userView()
        {
            try
            {
                if (Session["userName"] != null)
                {
                    userOperation operation = new userOperation();
                    var s = operation.GetAllUser();
                    return View(s);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }catch(Exception err)
            {
                return RedirectToAction("busView");
            }
        }

        public ActionResult createUser()
        {
            try
            {
                if (Session["userName"] != null)
                {
                    List<roleTable> list = dc.roleTable.ToList();
                    ViewBag.deptlist = new SelectList(list, "roleno", "rolename");
                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch(Exception e)
            {
                return RedirectToAction("userView");
            }
        }

        [HttpPost]
        public ActionResult createUser(userDTO user)
        {
            try
            {
                if (Session["userName"] != null)
                {
                    userOperation operation = new userOperation();
                    var s = operation.AddUser(user);
                    if (s == true)
                    {
                        // return RedirectToAction("userView");
                        var rolename = operation.getRoleName(user.roleId);

                        //if (string.Equals(rolename, "Student"))
                        if (rolename == "student" || rolename == "Student")
                        {
                            var d = operation.checkExistance(user.userId);
                            if (d == false)
                            {
                                TempData["message"] = "User Details successfully added.Please enter corresponding student details with same ID";
                                return RedirectToAction("createStudent");
                            }
                            else
                                return RedirectToAction("userView");
                        }
                        else
                            return RedirectToAction("userView");
                    }
                    else
                    {
                        TempData["Message"] = "Please Enter the Valid Details...";
                        List<roleTable> list = dc.roleTable.ToList();
                        ViewBag.deptlist = new SelectList(list, "roleno", "rolename");
                        //return View();
                        return RedirectToAction("createUser");
                    }
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }catch(Exception err)
            {
                List<roleTable> list = dc.roleTable.ToList();
                ViewBag.deptlist = new SelectList(list, "roleno", "rolename");
                return RedirectToAction("createUser");
            }
        }
        public ActionResult deleteUser(string userId)
        {
            try
            {
                if (Session["userName"] != null)
                {
                    if (userId == "1")
                    {
                        TempData["Message"] = "You are not allowed to Delete this Admin!!!";
                        return RedirectToAction("userView");
                    }
                    else
                    {
                        userOperation oper = new userOperation();
                        oper.DeleteUser(userId);
                        return RedirectToAction("userView");
                    }
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch(Exception err)
            {
                return RedirectToAction("userView");
            }
        }
        public ActionResult editUser(string userId)
        {
            try
            {
                if (Session["userName"] != null)
                {
                    List<roleTable> list = dc.roleTable.ToList();
                    ViewBag.deptlist = new SelectList(list, "roleno", "rolename");
                    userOperation oper = new userOperation();
                    var s = oper.EditUser(userId);
                    return View(s);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }catch(Exception err)
            {
                return RedirectToAction("userView");
            }

        }
        [HttpPost]
        public ActionResult editUser(userDTO dto)
        {
            try
            {
                if (Session["userName"] != null)
                {
                    userOperation oper = new userOperation();
                    var s = oper.SaveUser(dto);
                    if (s != true)
                    {
                        TempData["message"] = "Please enter the valid details...";
                        return View();
                    }
                    else
                        return RedirectToAction("userView");
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }
            catch (Exception err)
            {
                return RedirectToAction("userView");
            }
        }


        public ActionResult busView()
        {
            try
            {
                if (Session["userName"] != null)
                {
                    busOperation operation = new busOperation();
                    //var s = operation.GetBusDetails();
                    var s = operation.GetBusDetailsWithRoute();
                    return View(s);
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }catch(Exception err)
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult createBus()
        {
            try
            {
                if (Session["userName"] != null)
                {
                    var myResult = (from data in dc.routeTable.AsEnumerable()
                                    join lang in dc.routeTable.AsEnumerable()
                                    on data.routeId equals lang.routeId
                                    select new routeTable()
                                    {
                                        routeId = data.routeId,
                                        destination = "Source: " + data.source + " Destination: " + data.destination + " Via: " + data.via
                                        //LanguageName = lang.LanguageName
                                    }).ToList();
                    ViewBag.deptlist = new SelectList(myResult, "routeId", "destination");

                    return View();
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }catch(Exception err)
            {
                return RedirectToAction("busView");
            }
        }
        [HttpPost]
        public ActionResult createBus(busDTO bus)
        {
            try
            {
                if (Session["userName"] != null)
                {
                    busOperation operation = new busOperation();
                    var s = operation.addBusDetails(bus);
                    if (s == true)
                        return RedirectToAction("busView");
                    else
                    {
                        TempData["message"] = "Please enter the valid details...";
                        return RedirectToAction("createBus");
                    }
                }
                else
                {
                    return RedirectToAction("Index");
                }
            }catch(Exception err)
            {
                return RedirectToAction("busView");
            }
        }
        public ActionResult deleteBus(String busId)
        {
            if (Session["userName"] != null)
            {
                busOperation oper = new busOperation();
                var s=oper.deleteBusDetails(busId);
                if(s==true)
                    return RedirectToAction("busView");
                else
                {
                    TempData["message"] = "unable to delete this bus information due to dependencies.";
                    return RedirectToAction("busView");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult editBus(String busId)
        {
            if (Session["userName"] != null)
            {
                               var myResult = (from data in dc.routeTable.AsEnumerable()
                                join lang in dc.routeTable.AsEnumerable()
                                on data.routeId equals lang.routeId
                                select new routeTable()
                                {
                                    routeId = data.routeId,
                                    destination = "Source: " + data.source + " Destination: " + data.destination + " Via: " + data.via
                                    //LanguageName = lang.LanguageName
                                }).ToList();
                ViewBag.deptlist = new SelectList(myResult, "routeId", "destination");

                busOperation oper = new busOperation();
                var s = oper.EditUser(busId);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult editBus(busDTO dto)
        {
            if (Session["userName"] != null)
            {
                busOperation oper = new busOperation();
                var s = oper.updateBusDetails(dto);
                if (s != true)
                {
                    var myResult = (from data in dc.routeTable.AsEnumerable()
                                    join lang in dc.routeTable.AsEnumerable()
                                    on data.routeId equals lang.routeId
                                    select new routeTable()
                                    {
                                        routeId = data.routeId,
                                        destination = "Source: " + data.source + " Destination: " + data.destination + " Via: " + data.via
                                        //LanguageName = lang.LanguageName
                                    }).ToList();
                    ViewBag.deptlist = new SelectList(myResult, "routeId", "destination");
                    TempData["message"] = "Please enter the valid details...";
                    return View();
                    
                    
                }
                else
                    return RedirectToAction("busView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult routeView()
        {
            if (Session["userName"] != null)
            {
                routeOperation operation = new routeOperation();
                var s = operation.Display();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult createRoute()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult createRoute(routeDTO route)
        {
            if (Session["userName"] != null)
            {
                if (ModelState.IsValid)
                {
                    routeOperation operation = new routeOperation();
                    var s = operation.Add(route);
                    if (s == true)
                        return RedirectToAction("routeView");
                    else
                    {
                        TempData["Message"] = "Please enter the valid details...";
                        return RedirectToAction("createRoute");
                    }
                }
                else
                {
                    TempData["Message"] = "Please enter the valid details...";
                    return RedirectToAction("createRoute");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult deleteRoute(int routeId)
        {
            if (Session["userName"] != null)
            {
                routeOperation operation = new routeOperation();
                var deleteFlag = operation.delete(routeId);
                if(deleteFlag == true)
                return RedirectToAction("routeView");
                else
                {
                    TempData["message"] = "Unable to delete this route due to dependencies.";
                    return RedirectToAction("routeView");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult editRoute(int routeId)
        {
            if (Session["userName"] != null)
            {
                routeOperation operation = new routeOperation();
                var s = operation.Editroute(routeId);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult editRoute(routeDTO dto)
        {
            if (Session["userName"] != null)
            {
                routeOperation oper = new routeOperation();
                var s= oper.updateroute(dto);
                if (s != true)
                {
                    TempData["message"] = "Please enter the valid details...";
                    return View();
                }
                else
                    return RedirectToAction("routeView");
            }
            else
            {
                TempData["message"] = "Please enter the valid details...";
                return View();
            }
        
        }
        public ActionResult roleView()
        {
            if (Session["userName"] != null)
            {
                roleOperation operation = new roleOperation();
                var s = operation.GetAllrole();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult createRole()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult createRole(roleDTO role)
        {
            if (Session["userName"] != null)
            {
                if (ModelState.IsValid)
                {
                    roleOperation operation = new roleOperation();
                    var s = operation.saveroleTable(role);
                    if (s == true)
                        return RedirectToAction("roleView");
                    else
                    {
                        TempData["message"] = "Please enter the valid details...";
                        return RedirectToAction("createRole");
                    }
                }
                else
                {
                    TempData["message"] = "Please enter the valid details...";
                    return RedirectToAction("createRole");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult editRole(int roleno)
        {
            if (Session["userName"] != null)
            {

                roleOperation operation = new roleOperation();
                var s = operation.Editrole(roleno);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult editRole(roleDTO dto)
        {
            if (Session["userName"] != null)
            {
                if (ModelState.IsValid)
                {
                    roleOperation oper = new roleOperation();
                    var s = oper.updaterole(dto);
                    if (s != true)
                    {
                        TempData["message"] = "Please enter the valid details...";
                        return View();
                    }
                    else
                        return RedirectToAction("roleView");
                }
                else
                {
                    TempData["message"] = "Please enter the valid details...";
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        public ActionResult deleteRole(int roleno)
        {
            if (Session["userName"] != null)
            {
                roleOperation operation = new roleOperation();
                var flag = operation.deleteroleTable(roleno);
                if(flag == true)
                return RedirectToAction("roleView");
                else
                {
                    TempData["message"] = "unable to delete this Role due to dependencies";
                    return RedirectToAction("roleView");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult departmentView()
        {
            if (Session["userName"] != null)
            {
                departmentOperation operation = new departmentOperation();
                var s = operation.GetAlldepartment();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult createDepartment()
        {
            if (Session["userName"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult createDepartment(departmentDTO department)
        {
            if (Session["userName"] != null)
            {
                if (ModelState.IsValid)
                {
                    departmentOperation operation = new departmentOperation();
                    var s = operation.savedepartmentTable(department);
                    if (s == true)
                        return RedirectToAction("departmentView");
                    else
                    {
                        TempData["message"] = "Please enter the valid details...";
                        return RedirectToAction("createDepartment");
                    }
                    
                }
                else
                {
                    TempData["message"] = "Please enter the valid details...";
                    return RedirectToAction("createDepartment");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult editDepartment(int deptno)
        {
            if (Session["userName"] != null)
            {
                departmentOperation operation = new departmentOperation();
                var s = operation.Editdepartment(deptno);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult editDepartment(departmentDTO dto)
        {
            if (Session["userName"] != null)
            {
                if (ModelState.IsValid)
                {
                    departmentOperation oper = new departmentOperation();
                    var s = oper.updatedepartment(dto);
                    if (s != true)
                    {
                        TempData["message"] = "Please enter the valid details...";
                        return View();
                    }
                    else
                        return RedirectToAction("departmentView");
                }
                else
                {
                    TempData["message"] = "Please enter the valid details...";
                    return View();
                }
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        public ActionResult deleteDepartment(int deptno)
        {
            if (Session["userName"] != null)
            {
                departmentOperation operation = new departmentOperation();
                var flag = operation.deletedepartmentTable(deptno);
                if(flag == true)
                return RedirectToAction("departmentView");
                else
                {
                    TempData["message"] = "unable to delete this Department due to dependencies";
                    return RedirectToAction("departmentView");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }


        public ActionResult studentView()
        {
            if (Session["userName"] != null)
            {
                studentOperation operation = new studentOperation();
                var s = operation.viewstudentDetails();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult createStudent()
        {
            if (Session["userName"] != null)
            {
                List<departmentTable> list = dc.departmentTable.ToList();
                ViewBag.deptlist = new SelectList(list, "deptno", "deptname");
                var myResult = (from data in dc.busTable.AsEnumerable()
                                join lang in dc.busTable.AsEnumerable()
                                on data.busId equals lang.busId
                                select new selectbus()
                                {
                                    busId = data.busId,
                                    routeId =data.busId
                                }).ToList();
                ViewBag.studlist = new SelectList(myResult, "busId", "routeId");

                return View();
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        [HttpPost]
        public ActionResult createStudent(studentDTO student)
        {
            if (Session["userName"] != null)
            {
                studentOperation operation = new studentOperation();
                var s = operation.addstudentTable(student);
                if (s == true) {
                   ///TempData["message"] = "Student details successfully added.Please add corresponding user Details with same user ID, if not done earlier.";
                   //return RedirectToAction("createUser");
                    ///return RedirectToAction("studentView");
                    var d = operation.checkUserExistance(student.userId);
                    if (d == false)
                    {
                        TempData["message"] = "Student Details successfully added.Please enter corresponding user details with same ID";
                        return RedirectToAction("createUser");
                    }
                    else
                        return RedirectToAction("studentView");
                }
                else
                {
                    TempData["message"] = "Please enter the valid details...";
                    return RedirectToAction("createStudent");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        public ActionResult deleteStudent(string userId)
        {
            if (Session["userName"] != null)
            {
                studentOperation oper = new studentOperation();
                oper.deletestudentDetails(userId);
                return RedirectToAction("studentView");
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        public ActionResult editStudent(string userId)
        {
            if (Session["userName"] != null)
            {
                List<departmentTable> list = dc.departmentTable.ToList();
                ViewBag.deptlist = new SelectList(list, "deptno", "deptname");
                var myResult = (from data in dc.busTable.AsEnumerable()
                                join lang in dc.busTable.AsEnumerable()
                                on data.busId equals lang.busId
                                select new selectbus()
                                {
                                    busId = data.busId,
                                    routeId = data.busId,
                                }).ToList();
                ViewBag.studlist = new SelectList(myResult, "busId", "routeId");

                studentOperation oper = new studentOperation();
                var s = oper.editstudentdetails(userId);
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }
        }
        [HttpPost]
        public ActionResult editStudent(studentDTO dto)
        {
            if (Session["userName"] != null)
            {
                studentOperation oper = new studentOperation();
                var s = oper.edit(dto);
                if (s != true)
                {
                    TempData["message"] = "Please enter the valid details...";
                    return View();
                }
                else
                    return RedirectToAction("studentView");
            }
            else
            {
                TempData["message"] = "Please enter the valid details...";
                return View();
            }
        }

        public ActionResult locationView()
        {
            if (Session["userName"] != null)
            {
                locationOperation oper = new locationOperation();
                var s = oper.getlocationDetails();
                return View(s);
            }
            else
            {
                return RedirectToAction("Index");
            }

        }
        public ActionResult alterbus()
        {
            busOperation b = new busOperation();
            return View(b.alterReady());
        }

        public ActionResult Ready(String busId)
        {
            busOperation b = new busOperation();
            b.updatealterbus(busId);
            return RedirectToAction("alterbus");
        }

        public ActionResult unlockUser()
        {
            if (Session["userName"] != null)
            {
                userOperation user = new userOperation();
                return View(user.unlock());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult userUnlocked(string userId)
        {
            if (Session["userName"] != null)
            {
                userOperation us = new userOperation();
                var b = us.updateuserStatus(userId);
                if (b == true)
                {
                    TempData["message"] = "Unlocked successfully";
                    return RedirectToAction("unlockUser");
                }
                else
                {
                    TempData["message"] = "Failed to Unlock!! Try again";
                    return RedirectToAction("unlockUser");
                }
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult unlockBus()
        {
            if (Session["userName"] != null)
            {
                busOperation user = new busOperation();
                return View(user.unlock());
            }
            else
            {
                return RedirectToAction("Index");
            }
        }

        public ActionResult busUnlocked(string busId)
        {
            if (Session["userName"] != null)
            {
                busOperation us = new busOperation();
                var p = us.updatebusStatus(busId);
                if (p == true)
                {
                    TempData["message"] = "Unlocked successfully";
                    return RedirectToAction("unlockBus");
                }
                else
                {
                    TempData["message"] = "Failed to Unlocked!! try again!! ";
                    return RedirectToAction("unlockBus");
                }

            }
            else
            {
                return RedirectToAction("Index");
            }
        }
    }
    public class selectbus
    {
       public string busId { get; set; }
       public string routeId { get; set; }
    }
}
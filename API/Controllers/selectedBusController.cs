﻿using Data;
using Data.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class selectedBusController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage allBus()
        {
            dataContext dc = new dataContext();
            busOperation bo = new busOperation();
            var result = bo.availBusDetails();
            List<para> listName = new List<para>();
            foreach (var i in result)
            {
                if (i.status == 0)
                {
                    var routeRow = dc.routeTable.FirstOrDefault(x => x.routeId == i.routeId);
                    listName.Add(new para() { busId = i.busId });
                }
            }
            return Request.CreateResponse(HttpStatusCode.OK, listName);
        }



        [HttpPost]
        public HttpResponseMessage busInfo(bus b)
        {
            dataContext dc = new dataContext();
            try
            {
                var userRow = dc.studentTable.FirstOrDefault(x => x.userId == b.userId);
                string user = userRow.busId;
                var busRow = dc.busTable.FirstOrDefault(x => x.busId == user);
                var routeRow = dc.routeTable.FirstOrDefault(x => busRow.routeId == x.routeId);


                if (busRow.status == 0)
                {
                    var temp = dc.busTable.FirstOrDefault(x => x.routeId == routeRow.routeId && x.busId != busRow.busId && x.status == 1);
                    tempclass t = new tempclass();
                    if (temp != null)
                    {
                        var temproute = dc.routeTable.FirstOrDefault(x => busRow.routeId == x.routeId);
                        t.tempbusId = temp.busId;
                        t.tempsource = temproute.source;
                        t.tempvia = temproute.via;
                        t.tempdestinaton = temproute.destination;
                    }
                    else
                    {
                        t.tempbusId = "No";
                        t.tempsource = "No";
                        t.tempvia = "No";
                        t.tempdestinaton = "No";
                    }
                    return Request.CreateResponse(HttpStatusCode.OK, DateTime.Now.ToString("dd-MM-yyyy") + "," + DateTime.Now.TimeOfDay + "," + busRow.busId + "," + routeRow.source + "," + routeRow.via + "," + routeRow.destination + "," + t.tempbusId + "," + t.tempsource + "," + t.tempvia + "," + t.tempdestinaton);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, DateTime.Now.ToString("dd-MM-yyyy") + "," + DateTime.Now.TimeOfDay + "," + busRow.busId + "," + routeRow.source + "," + routeRow.via + "," + routeRow.destination + "," + "No" + "," + "No" + "," + "No" + "," + "No");
                }




            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, e);
            }
        }



        public class bus
        {
            public string userId { get; set; }
        }
        public class tempclass
        {
            public string tempbusId { get; set; }
            public string tempsource { get; set; }
            public string tempvia { get; set; }

            public string tempdestinaton { get; set; }
        }

        public class para
        {
            public string busId { get; set; }
        }

    }
}
﻿using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class checkBusAvailabilityController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage checkBus(userBus bus)
        {
            try
            {
                dataContext dc = new dataContext();
                var userRow = dc.userTable.FirstOrDefault(x => x.userId == bus.userId);
                var busRow = dc.busTable.FirstOrDefault(x => x.busId.Equals(bus.busId));
                if (busRow.status == 0)
                {
                    busRow.status = 1;
                    dc.busTable.Attach(busRow);
                    dc.Entry(busRow).State = EntityState.Modified;
                    dc.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, "success");
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");

            }
            catch (Exception err)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
        }
    }
    public class userBus
    {
        public string userId { get; set; }
        public string busId { get; set; }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class studentLogoutController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage logout(userLogout l)
        {
            return Request.CreateResponse(HttpStatusCode.OK, "success");
        }
        public class userLogout
        {
            public string userName { get; set; }
            public string userId { get; set; }
        }
    }
}

﻿using Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class summaryController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage summary(gettingrequest gr)
        {
            try
            {
                dataContext dc = new dataContext();
                var busRow = dc.busTable.FirstOrDefault(x => x.busId == gr.busId);
                var routeRow = dc.routeTable.FirstOrDefault(x => x.routeId == busRow.routeId);
                var start = dc.actualbusTimes.FirstOrDefault(x => x.busId == gr.busId);
                TimeSpan actualtime = DateTime.Parse(start.endtime).Subtract(DateTime.Parse(start.starttime));
                return Request.CreateResponse(HttpStatusCode.OK, DateTime.Now.Date + "," + gr.busId + "," + routeRow.plannedTime + "," + actualtime + "," + start.starttime + "," + start.endtime + "," + routeRow.source + "," + routeRow.via + "," + routeRow.destination);
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
        }
    }
    public class gettingrequest
    {
        public string userId { get; set; }
        public string busId { get; set; }
    }
}

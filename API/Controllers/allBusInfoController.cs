﻿using Data;
using Data.Operations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class allBusInfoController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage busInfo()
        {
            try
            {
                dataContext dc = new dataContext();
                busOperation bo = new busOperation();
                var result = bo.availBusDetails();
                List<para1> listName = new List<para1>();
                foreach (var i in result)
                {
                    if (i.status != 0)
                    {
                        var routeRow = dc.routeTable.FirstOrDefault(x => x.routeId == i.routeId);
                        listName.Add(new para1() { busId = i.busId, source = routeRow.source, via = routeRow.via, destination = routeRow.destination });
                    }
                    
                }

                if (!listName.Any())
                {
                    // return Request.CreateResponse(HttpStatusCode.OK, "NoBuses");
                    listName.Add(new para1() { busId = null, source = null, via = null, destination = null });
                    return Request.CreateResponse(HttpStatusCode.OK, listName);

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, listName);
                }
            }
            catch(Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
        }
    }

    public class para1
    {
        public string busId { get; set; }
        public string source { get; set; }
        public string via { get; set; }
        public string destination { get; set; }
    }
    public class buslist
    {
        public string  busId { get; set; }

    }
}


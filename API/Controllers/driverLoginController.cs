﻿using Data;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace API.Controllers
{
    public class driverLoginController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage login(user ul)
        {
            dataContext dc = new dataContext();
            var data = dc.userTable.Where(x => x.userName == ul.userName && x.password == ul.password).ToArray();
            var dto = dc.userTable.FirstOrDefault(x => x.userName == ul.userName && x.password == ul.password);
            try
            {
                if (dto != null)
                {
                    if (data.Any(u => u.userName == ul.userName && u.password == ul.password && u.roleId == 2&&u.status==0))
                    {
                        dto.status = 1;
                        dc.Entry(dto).State = EntityState.Modified;
                        dc.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, dto.userName + "," + dto.userId);
                    }
                    else if (data.Any(u => u.userName == ul.userName && u.password == ul.password && u.roleId == 2 && u.status == 1))
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "already");
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
                }
            }
            catch (Exception e)
            {
                return Request.CreateResponse(HttpStatusCode.OK, "unsuccess");
            }
        }
    }
}